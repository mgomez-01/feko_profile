#!/bin/bash

echo -e "
Establish an ssh connection to a computer with access to the CADE license
servers with the -D 8080 option to setup the SOCKS5 proxy for license
server access. Ping something \(like 8.8.8.8\) to keep the connection
alive. Press ctrl+A, D once this is done.

Press enter to get the prompt."

read

screen -S socks_proxy bash

sudo systemctl restart redsocks

echo -e "All done

Possible issues with lic server may exist. If no connection, use GlobalProtect VPN
and sign in before trying again.

GlobalProtect VPN deb file can be downloaded from here:
https://drive.google.com/file/d/1EP4oGyGcQm5gTr62wZiuI9RHIJWhKt0s/view?usp=drive_link

or rerunning this again if there was a problem

wget --output-document=gpvpn.deb \"https://drive.google.com/uc?export=download&id=1EP4oGyGcQm5gTr62wZiuI9RHIJWhKt0s&confirm=yes\"

followed by sudo dpkg -i /opt/gpvpn.deb

"

cd /opt/ && sudo wget --output-document=gpvpn.deb "https://drive.google.com/uc?export=download&id=1EP4oGyGcQm5gTr62wZiuI9RHIJWhKt0s&confirm=yes"

sudo dpkg -i /opt/gpvpn.deb

echo "finished Lic setup "
