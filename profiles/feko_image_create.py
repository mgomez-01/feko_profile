"""
Create image for FEKO Profile.
Mounts the dataset as read/write as opposed to the normal read-only.
"""

import profiles.profile_base

DATASET = \
    "urn:publicid:IDN+emulab.net:powdersandbox+ltdataset+feko-2022-build"

profiles.profile_base.make_profile(
    "urn:publicid:IDN+emulab.net+image+antennas:feko_2022_cst_clone",
    DATASET,
    True
)
